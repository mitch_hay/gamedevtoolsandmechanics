# GameDevToolsAndMechanics
### A collection of Unity Tools and Mechanics developed by Mitchell Hayward 

Within this repository are various collections of tools and mechanics developed utilising the Unity game engine, this repository can be treated as an example/portfolio of programming/gameplay solutions for Game Development and is **NOT** free to utilise.

This includes:

- [2D Day Night Cycle](https://bitbucket.org/mitch_hay/gamedevtoolsandmechanics/src/master/DayNightCycle/dayNightCycle.cs "2D Day/Night Cycle Unity") using the Color.Lerp functionality (see: https://docs.unity3d.com/ScriptReference/Color.Lerp.html for relevant documentation) | **Project**: Bin Chicken Mayhem 2019

- [Mobile Joystick Movement](https://bitbucket.org/mitch_hay/gamedevtoolsandmechanics/src/master/JoyStickPlayerMovementMobile/Joystick.cs "Mobile Joystick Movement Unity") utilising Unity's standard asset CrossPlatformInput to register mouse inputs as touch/drag events | **Project**: Bin Chicken Mayhem 2019

- [Player Controller](https://bitbucket.org/mitch_hay/gamedevtoolsandmechanics/src/master/JoyStickPlayerMovementMobile/PlayerMovementController/playerController.cs "Player Controller Unity") using a 2-Dimensional Rigidbody, dynamically setting a Movement Force (depending on selected play sensitivity and player state) to mimic flying/gliding | **Project**: Bin Chicken Mayhem 2019
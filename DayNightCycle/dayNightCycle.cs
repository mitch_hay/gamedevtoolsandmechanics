﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class dayNightCycle : MonoBehaviour
{

    /* 
     * Mechanic: 2D Day Night Cycle with Image / SpriteRenderer
     * Developed by: Mitchell Hayward
     * Project: Bin Chicken Mayhem (iOS)
     * NOTE: This code is NOT for free use and is protected under relevant Copyright Law
    */

    public bool isLevelMenu = false;
    public bool isMayhemMenu = false;

    // Set how long the days last in-game (seconds)
    public float duration;

    public Color morning;
    public Color day;
    public Color night;
    public Light cloudLighting;

    // Set these in Unity to determine what time you want to start at (e.g. if game scene starts at night isNight = true)
    public bool isMorning;
    public bool isDay;
    public bool isNight;

    private float t; // When t = 0 Color is = a, and when t = 1 Color Lerps to b (e.g. Color.Lerp(Color a, Color b, t))

    public SpriteRenderer sky;
    public Image skyImage;

    // Update is called once per frame
    void Update()
    {
        // Three possible times of day: morning, day, and night; set the colour transition with the dayCycle() function depending on the time of day

        if (isMorning)
        {
            dayCycle(night, morning);
        }
        else if (isDay)
        {
            dayCycle(morning, day);
        }
        else if (isNight)
        {
            dayCycle(day, night);
        }
    }

    public void dayCycle(Color a, Color b)
    {
        if (isMayhemMenu)
        {
            skyImage.color = Color.Lerp(a, b, t);
        }
        else
        {
            sky.color = Color.Lerp(a, b, t);
        }

        if (isLevelMenu)
        {
            if (isMorning)
            {
                cloudLighting.intensity = Mathf.Lerp(0f, 1f, t);
            }
            else if (isNight)
            {
                cloudLighting.intensity = Mathf.Lerp(1f, 0f, t);
            }
        }

        if (t < 1)
        {
            t += Time.deltaTime / duration;
        }
        else if (t >= 1)
        {
            t = 0;

            // Set time of day when t = 0
            if (isMorning)
            {
                isMorning = false;
                isDay = true;
            }
            else if (isDay)
            {
                isDay = false;
                isNight = true;
            }
            else if (isNight)
            {
                isNight = false;
                isMorning = true;
            }
        }
    }
}

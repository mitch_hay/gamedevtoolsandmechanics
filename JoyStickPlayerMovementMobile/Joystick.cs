using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UnityStandardAssets.CrossPlatformInput
{
	public class Joystick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
	{
        /*
         * Mechanic: Joystick Movement with Player Tilt (Mobile)
         * Project: Bin Chicken Mayhem
         * Developed by: Mitchell Hayward
        */

		public enum AxisOption
		{
			// Options for which axes to use
			Both, // Use both
			OnlyHorizontal, // Only horizontal
			OnlyVertical // Only vertical
		}

		public int MovementRange = 100;
		public AxisOption axesToUse = AxisOption.Both; // The options for the axes that the still will use
		public string horizontalAxisName = "Horizontal"; // The name given to the horizontal axis for the cross platform input
		public string verticalAxisName = "Vertical"; // The name given to the vertical axis for the cross platform input
        public bool isDev = false; // Used to define whether developer mode is activated (i.e. can freely set certain parameters in-game)

		Vector3 m_StartPos;
		bool m_UseX; // Toggle for using the x axis
		bool m_UseY; // Toggle for using the Y axis
		CrossPlatformInputManager.VirtualAxis m_HorizontalVirtualAxis; // Reference to the joystick in the cross platform input
		CrossPlatformInputManager.VirtualAxis m_VerticalVirtualAxis; // Reference to the joystick in the cross platform input

		public float tiltSpeed;
		public float tiltAngle;
		public Quaternion tiltRotation;
		public Vector2 mousePosition;
		public Transform tiltTarget;

		public float upAngleLimit;
		public float downAngleLimit;

		void OnEnable()
		{
			CreateVirtualAxes();
		}

        void Start()
        {
            m_StartPos = transform.position;

            if (isDev)
            {
                upAngleLimit = PlayerPrefs.GetInt("upAngle");
                downAngleLimit = PlayerPrefs.GetInt("downAngle");
            } else {
                upAngleLimit = 45;
                downAngleLimit = -45;
                PlayerPrefs.SetInt("upAngle", (int)upAngleLimit);
                PlayerPrefs.SetInt("downAngle", (int)downAngleLimit);
            }
        }

		void UpdateVirtualAxes(Vector3 value)
		{
			var delta = m_StartPos - value;
			delta.y = -delta.y;
			delta /= MovementRange;
			if (m_UseX)
			{
				m_HorizontalVirtualAxis.Update(-delta.x);
			}

			if (m_UseY)
			{
				m_VerticalVirtualAxis.Update(delta.y);
			}
		}

		void CreateVirtualAxes()
		{
			// set axes to use
			m_UseX = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyHorizontal);
			m_UseY = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyVertical);

			// create new axes based on axes to use
			if (m_UseX)
			{
				m_HorizontalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(horizontalAxisName);
				CrossPlatformInputManager.RegisterVirtualAxis(m_HorizontalVirtualAxis);
			}
			if (m_UseY)
			{
				m_VerticalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(verticalAxisName);
				CrossPlatformInputManager.RegisterVirtualAxis(m_VerticalVirtualAxis);
			}
		}


		public void OnDrag(PointerEventData data)
		{
			Vector3 newPos = Vector3.zero;

			if (m_UseX)
			{
				int delta = (int)(data.position.x - m_StartPos.x);
				//delta = Mathf.Clamp(delta, - MovementRange, MovementRange);
				newPos.x = delta;
			}

			if (m_UseY)
			{
				int delta = (int)(data.position.y - m_StartPos.y);
				delta = Mathf.Clamp(delta, -MovementRange, MovementRange);
				newPos.y = delta;
				mousePosition = (Camera.main.ScreenToWorldPoint(Input.mousePosition) * 100) - tiltTarget.position; // RETRIEVE MOUSE COORDINATES
				tiltAngle = Mathf.Atan2(mousePosition.y, mousePosition.x)*Mathf.Rad2Deg; // DETERMINE THE FLIGHT ANGLE
				
				// SET FLIGHT ANGLE LIMITS

				if (tiltAngle > upAngleLimit) {
					tiltAngle = upAngleLimit;
				} else if (tiltAngle < downAngleLimit) {
					tiltAngle = downAngleLimit;
				}

				tiltRotation = Quaternion.AngleAxis(tiltAngle, Vector3.forward); // CREATE A ROTATION VALUE FOR THE PLAYER
				tiltTarget.rotation = Quaternion.Slerp(tiltTarget.rotation, tiltRotation, tiltSpeed * Time.unscaledDeltaTime);
			}
			transform.position = new Vector3(m_StartPos.x + newPos.x, m_StartPos.y + newPos.y, m_StartPos.z + newPos.z);
			UpdateVirtualAxes(transform.position);
		}


		public void OnPointerUp(PointerEventData data)
		{
			transform.position = m_StartPos;
			UpdateVirtualAxes(m_StartPos);
			tiltAngle = 0;
			tiltTarget.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
			tiltRotation = Quaternion.AngleAxis(tiltAngle, Vector3.forward); // CREATE A ROTATION VALUE FOR THE PLAYER
			tiltTarget.rotation = Quaternion.Slerp(tiltTarget.rotation, tiltRotation, tiltSpeed * Time.unscaledDeltaTime);
		}


		public void OnPointerDown(PointerEventData data) { }

		void OnDisable()
		{
			// remove the joysticks from the cross platform input
			if (m_UseX)
			{
				m_HorizontalVirtualAxis.Remove();
			}
			if (m_UseY)
			{
				m_VerticalVirtualAxis.Remove();
			}
		}
	}
}
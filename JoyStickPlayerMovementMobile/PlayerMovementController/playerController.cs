﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.UI;

public class playerController : MonoBehaviour 
{
    /*
     * Mechanic: 2D Player Controller (Mobile)
     * Project: Bin Chicken Mayhem
     * Developed by: Mitchell Hayward
    */

    public int sensitivity;
	public float moveForce;
	public float boostZone = 2;

	public Scrollbar sb;
	public Text sbValue;

	Rigidbody2D myBody;

    public int isBagged;
    public float baggedTime = 5f;
    public float baggedTimeStatic = 5f;

    public Image timeLeft;
    public GameObject escapeButton;
    public Animator powerUpsBar;

    public GameObject[] whatToSpawn;
    public GameObject[] prefabEffect;
	
	void Start () 
	{
		myBody = this.GetComponent<Rigidbody2D>();
        isBagged = 0;
        PlayerPrefs.SetInt("isBagged", isBagged); // Set isBagged to 0 on start (isBagged being the scenario when the player collides with a plastic bag)
	}
	
	void Update () 
	{
        sensitivity = PlayerPrefs.GetInt("sensitivity");
        isBagged = PlayerPrefs.GetInt("isBagged");

        if (isBagged == 1)
        {
            escapeButton.SetActive(true);
            powerUpsBar.SetInteger("down", 1);
            baggedTime -= Time.deltaTime;
            moveForce = 0; // Set the moveForce to 0 when player is trapped in plastic bag (i.e. the player will not be able to move on x and y axis)
            gameObject.GetComponent<Animator>().SetInteger("isBagged", isBagged);
            timeLeft.fillAmount = baggedTime / baggedTimeStatic; 

            if (baggedTime <= 0)
            {
                isBagged = 0;
                PlayerPrefs.SetInt("isBagged", isBagged);
                SpawnEffect(); // Spawn the bag break effect 
                baggedTime = baggedTimeStatic;
            }
        }
        else
        {
            moveForce = sensitivity * 10; // Set the moveForce so the player can move on x and y axis again
            gameObject.GetComponent<Animator>().SetInteger("isBagged", isBagged);
            powerUpsBar.SetInteger("down", 0);
            escapeButton.SetActive(false);
        }

		Vector2 moveVector = new Vector2 (CrossPlatformInputManager.GetAxis("Horizontal"), CrossPlatformInputManager.GetAxis("Vertical")) * moveForce; 
		bool isBoost = CrossPlatformInputManager.GetButton ("Boost");
		myBody.AddForce (moveVector * (isBoost ? boostZone : 1));
	}

    public void escapeQuicker(){
        baggedTime -= 0.1f;
    }

    public void SpawnEffect()
    {
        whatToSpawn[0] = Instantiate(prefabEffect[0], gameObject.transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
    }
				
}
